

var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'flash', 'dashboard', 'wt.responsive', 'ngCookies', 'ngStorage','angucomplete-alt']);


app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $modalInstance, $localStorage,
    $sessionStorage) {
    //IdleScreenList
    $stateProvider
        .state('app', {
            url: '/app',
            templateUrl: 'app/common/app.html',
            controller: 'appCtrl',
            controllerAs: 'vm',
            data: {
                pageTitle: 'Login'
            }
        });

    $urlRouterProvider.otherwise('login');

    //$urlRouterProvider.otherwise('app/dashboard');
    //$urlRouterProvider.otherwise('/app/dashboard');
}]);

// set global configuration of application and it can be accessed by injecting appSettings in any modules
app.constant('appSettings', appConfig);

app.run(['$rootScope', '$state', 'AuthenticationService', '$cookieStore',
    function ($rootScope, $state, AuthenticationService, $cookieStore) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {



            if (toState.name !== 'login') {
                if ($rootScope.userAccess === undefined) {
                    AuthenticationService.setUserAccess($cookieStore.get("globals").currentUser.token).then(function () {
                        //
                    });
                }

                //console.log("checking", $rootScope.userAccess.roles.indexOf("User"));
                if ($rootScope.userAccess.roles.indexOf("User") < 0) {
                    console.log("User Role Not Present, access denied");
                    event.preventDefault();
                    $state.go('login');
                }
                else if (toState.data.grantAccessTo.indexOf('Admin') >= 0 && $rootScope.userAccess.roles.indexOf("Admin") < 0) {
                    event.preventDefault();
                    $state.go('login');
                }
                else if (toState.data.readAccess != undefined && $rootScope.userAccess.permissions.indexOf(toState.data.readAccess) < 0) {
                    event.preventDefault();
                    
                }
            }
        });

    }]
);