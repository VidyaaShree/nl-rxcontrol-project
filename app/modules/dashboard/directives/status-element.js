dashboard
		.directive(
				'status',
				function() {
					return {
						restrict : 'E',
						// replace : true,
						template : '<led id="{{status.id}}" title="{{status.label}}" class="{{status.led}}"></led>',
						scope : {
							'status' : '=data'
						},
						link : function(scope, elem, attrs) {
							console.log(scope.status);
						}
					}
				});