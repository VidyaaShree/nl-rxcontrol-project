﻿dashboard.controller("loginCtrl", [
	'$rootScope',
	'$scope',
	'$cookieStore',
	'$state',
	'$location',
	'Flash',
	'AuthenticationService',
	'$sessionStorage',
	'UsersService',
	function ($rootScope, $scope, $cookieStore, $state, $location, Flash,
		AuthenticationService, $sessionStorage, UsersService) {

		$scope.Events = {};
		$scope.Events.DashBoard = DashBoard;
		$scope.errorLogin = false;
		$scope.statusText = '';
		$sessionStorage.$reset();
		function DashBoard() {
			$scope.dataLoading = true;
			AuthenticationService.Login($scope.username, $scope.password,
				function (response) {
					if (response.status !== 200) {
						$scope.statusText = response.statusText;
						$scope.errorLogin = true;
						$scope.dataLoading = false;
					} else if (response.data && response.data.sub) {
						$rootScope.username = response.data.sub;
						AuthenticationService.SetCredentials(response.data.sub,
							response.data.token);
						$scope.errorLogin = false;
						AuthenticationService.setUserAccess(response.data.token).then(function () {

							$state.go('app.dashboard');
						});
					} else {
						$scope.statusText = 'Invalid Username or Password';
						$scope.errorLogin = true;
						$scope.dataLoading = false;
					}
				});
		}
	}]);
