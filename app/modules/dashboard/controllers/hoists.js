﻿﻿dashboard.controller("HoistController", [
    '$rootScope',
    '$scope',
    '$state',
    '$location',
    'HoistsService',
    'dashboardService',
    'Flash',
    '$timeout',
    '$interval',
    'AuthenticationService', '$sessionStorage', '$window', 'ControllersService', 'LocationService', 'CompanyService',
    function ($rootScope, $scope, $state, $location, HoistsService,
        dashboardService, Flash, $timeout, $interval,
        AuthenticationService, $sessionStorage, $window, ControllersService, LocationService, CompanyService) {
        $scope.hoistsData = [];
        $scope.dataLoading = true;
        $scope.numberToDisplay = 15;
        $scope.hoistFormData = {
            "hoistNumber": 0,
            "hoistType": "",
            "description": "",
            "note": "",
            "companyId": 0,
            "controllerId": null,
            "locationId": 0
        };


 
        $scope.originalHoistFormData = JSON.parse(JSON.stringify($scope.hoistFormData));
        $scope.autoCompleteData = {
            companyData: null,
            controllersData: null,
            locationData: null,
            hoistTypesData: null
        }
        $scope.companyData = [];
        $scope.locationData = [];
        $scope.controllersData = [];
        $scope.hoistTypesData = []

        $scope.showMessageModal = false;
        $scope.messageModalText = '';
        $scope.messageModalType = '';
        $scope.autoCompleteDataUnTouched = false;
        $scope.reloadAutoCompleteData = function () {
            HoistsService.controllersList().then(function (response) {
                $scope.autoCompleteData.controllersData = response;
                $scope.controllersData = JSON.parse(JSON.stringify($scope.autoCompleteData.controllersData));
                $scope.controllersData.unshift({ id: null, label: 'Select a controller' });
            }, function (msg) {
                $scope.dataLoading = false;

            });
            HoistsService.locationsList().then(function (response) {
                $scope.autoCompleteData.locationData = response;
                $scope.locationData = JSON.parse(JSON.stringify($scope.autoCompleteData.locationData));
                $scope.locationData.unshift({ id: null, label: 'Select a location' });
            }, function (msg) {
                $scope.dataLoading = false;
            });
            HoistsService.companiesList().then(function (response) {
                $scope.autoCompleteData.companyData = response;
                $scope.companyData = JSON.parse(JSON.stringify($scope.autoCompleteData.companyData));
                $scope.companyData.unshift({ id: null, label: 'Select a company' });
            }, function (msg) {
                //  console.log(msg);
                //  $scope.dataLoading = false;
            });
            HoistsService.hoistTypesList().then(function (response) {
                // console.log(response);
                response.forEach(function (type, index) {
                    $scope.hoistTypesData[index] = {
                        label: type,
                        id: type
                    }
                })
                $scope.hoistTypesData.unshift({ id: null, label: 'Select a hoist type' });
                //$scope.autoCompleteData.hoistTypesData = response;
                //$scope.hoistTypesData = JSON.parse(JSON.stringify($scope.autoCompleteData.hoistTypesData));
                // $scope.dataLoading = false;
            }, function (msg) {
                //  console.log(msg);
                //  $scope.dataLoading = false;
            });

        };
        $scope.reloadAutoCompleteData();


        $scope.selectedIndex = null;
        $scope.showModal = false;
        // column to sort
        $scope.column = 'hoistNumber';
        // sort ordering (Ascending or Descending). Set true for desending
        $scope.reverse = false;
        // $sessionStorage.sortOrderd = false;
        // called on header click
        $scope.sortColumn = function (col) {
            debugger;
            $scope.reverse = ($scope.column === col) ? !$scope.reverse : false;
            $scope.column = col;

            $sessionStorage.sortOrderd = $scope.reverse;
            $sessionStorage.sortcol = $scope.column;
            $scope.numberToDisplay = $scope.hoistsData.length;
        };
        if ($sessionStorage.sortOrderd != null && $sessionStorage.sortcol != null) {
            $scope.reverse = $sessionStorage.sortOrderd;
            $scope.column = $sessionStorage.sortcol;
        }

        $scope.setSearchData = function () {
            $sessionStorage.hoistSearchData = $scope.searchFish;
        }
        $scope.searchFish = $sessionStorage.hoistSearchData;


        $scope.getMoreData = function () {
            $scope.dataLoading = true;
            HoistsService.hoists().then(function (response) {
                $scope.hoistsData = response;
                $scope.hoistsData.forEach(function (hoistRow, index) {
                    $scope.hoistsData[index].statusIndex = index;
                    $scope.hoistsData[index].statusColor = 'grey';
                });
                $scope.numberToDisplay = $scope.hoistsData.length;
                if ($scope.searchFish != "" && $scope.searchFish != undefined) {
                    $scope.numberToDisplay = $scope.hoistsData.length;
                }
                $scope.dataLoading = false;
            }, function (msg) {
                console.log(msg);
                $scope.dataLoading = false;
            });
            //updateStatusTimeout();
        }
        $scope.getMoreData();
        $scope.Events = {};
        $scope.Events.viewChange = viewChange;
        $scope.Events.statusviewChange = statusviewChange;
        $scope.currentView = "listView.html";

        function viewChange(listval) {
            $scope.currentView = listval;
        }

        function statusviewChange(listval) {
            $scope.currentStatusView = listval;
        }

        $scope.navTo = function (id) {
            $state.go('app.HoistsDash', {
                hoistId: id
            });
        }

        /**
         * Update the status for all hoists in the list.
         */
        $scope.hoistStatuses = [];

        $scope.getById = function (id) {
            return $scope.hoistStatuses[id];
        };

        $scope.updateStatus = function () {
            var hoistIds = $scope.hoistsData.map(function (a) {
                return a.hoistId;
            });
            HoistsService.updateStatus(hoistIds).then(
                function (response) {
                    if (response.error == undefined) {

                        $scope.hoistStatuses = response.reduce(function (
                            result, next) {
                            result[next.id] = next;
                            return result;
                        }, {});
                        $scope.hoistsData.forEach(function (hoistRow, index) {
                            var hoistId = $scope.hoistsData[index].hoistId;
                            var statusById = $scope.hoistStatuses[hoistId];
                            $scope.hoistsData[index].statusIndex = statusById.led == 'grey' ? 2 : statusById.led == 'green' ? 1 : 0;
                            $scope.hoistsData[index].statusColor = statusById.led;
                        });
                    }
                },
                function () {

                });
        };

        var promise = null;
        var updateStatusTimeout = function () {
            $scope.updateStatus();
            promise = $timeout(updateStatusTimeout, 5 * 1000);
        };

        updateStatusTimeout();

        /*angular.element($window).bind("scroll", function() {
            //Below code load data while scroll reached at the bottom
            /*var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            var body = document.body, html = document.documentElement;
            var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
            windowBottom = windowHeight + window.pageYOffset;
            if (windowBottom >= docHeight) {
                $scope.numberToDisplay += 5;
            }
        });*/
        $scope.$on('$destroy', function () {
            $timeout.cancel(promise);
        });
        $scope.showHoistRowMenu = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.selectedIndex == index ? $scope.selectedIndex = null : $scope.selectedIndex = index;

        };
        $scope.addHoist = function () {
            $scope.formSubmitMessage = '';
            $scope.hoistFormData = {
                "hoistNumber": "",//$scope.hoistsData[$scope.hoistsData.length - 1].hoistNumber + 1
                "hoistType": "",
                "description": "",
                "note": "",
                "companyId": 0,
                "controllerId": null,
                "locationId": 0
            };
            $scope.originalHoistFormData = JSON.parse(JSON.stringify($scope.hoistFormData));
            $scope.$broadcast('angucomplete-alt:clearInput');
            //$scope.$broadcast('angucomplete-alt:changeInput', 'ex1', $scope.companyData[0]);
            //$scope.$broadcast('angucomplete-alt:changeInput', 'ex2', $scope.locationData[0]);

            $scope.showModal = !$scope.showModal;
            $scope.formAction = 'add';
        }
        $scope.setEditForm = function (companyName, locationName, controllerName, hoistTypeId) {
            if ($scope.companyData != undefined && $scope.companyData.length > 0) {
                //     $scope.editForm.selectedCompany = null;
                //   $scope.$broadcast('angucomplete-alt:changeInput', 'ex1', null);
                $scope.companyData.forEach(function (company, index) {
                    if (company.id == companyName) {
                        $scope.editForm.selectedCompany = company;
                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex1', company);

                        return;
                    }
                })
            }
            // $scope.editForm.selectedLocation = null;
            // $scope.$broadcast('angucomplete-alt:changeInput', 'ex2', null);
            $scope.locationData.forEach(function (location, index) {
                if (location.label == locationName) {
                    $scope.editForm.selectedLocation = location;
                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex2', location);
                    return;
                }
            });
            $scope.editForm.selectedcontroller = { id: null, label: null };
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex3', { id: null, label: null });
            $scope.controllersData.forEach(function (controller, index) {
                if (controller.id == controllerName) {
                    $scope.editForm.selectedcontroller = controller;
                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex3', controller);
                    return;
                }
            })
            // $scope.editForm.selectedHoistType = null;
            //$scope.$broadcast('angucomplete-alt:changeInput', 'ex0', null);
            $scope.hoistTypesData.forEach(function (hoistType, index) {
                if (hoistType.id == hoistTypeId) {
                    $scope.editForm.selectedHoistType = hoistType;
                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex0', hoistType);
                    return;
                }
            })

        };
        $scope.clearEditForm = function () {
            $scope.editForm = {
                selectedLocation: null,
                selectedCompany: null,
                selectedController: null,
                selectedHoistType: null
            }
        }
        $scope.editHoist = function (hoist) {
            $scope.formSubmitMessage = '';
            $scope.formAction = 'edit';
            $scope.selectedIndex = null;
            $scope.clearEditForm();
            $scope.hoistFormData.hoistId = hoist.hoistId;
            $scope.hoistFormData.hoistNumber = hoist.hoistNumber;
            $scope.hoistFormData.hoistType = hoist.hoistType;
            $scope.hoistFormData.description = hoist.description;
            $scope.hoistFormData.note = hoist.note;
            $scope.hoistFormData.locationId = hoist.locationId;
            $scope.hoistFormData.controllerId = hoist.controllerId;
            $scope.hoistFormData.companyId = hoist.companyId;
            $scope.hoistFormData.note = hoist.note;
            $scope.setEditForm(hoist.companyId, hoist.locationName, hoist.controllerId, hoist.hoistType);
            $scope.showModal = !$scope.showModal;
            $scope.originalHoistFormData = JSON.parse(JSON.stringify($scope.hoistFormData));
        }
        $scope.submitHoistForm = function (formData) {
            $scope.formSubmitMessage = '';
            var formValidate = true;
            if ($('#ex1_value').val() == "" || $('#ex1_value').val() == "Select a company" ) {
                $scope.formSubmitMessage = 'Company Fields is Required';
                formValidate = false;
            }
            else  if ($('#ex2_value').val() == "" || $('#ex2_value').val() == "Select a location") {
                $scope.formSubmitMessage = 'Location Fields is Required';
                formValidate = false;
            }
            else if ($scope.hoistFormData.companyId !== undefined || $scope.hoistFormData.locationId !== undefined) {

                if ($scope.hoistFormData.companyId.originalObject === undefined || $scope.hoistFormData.locationId.originalObject === undefined) {
                    //$scope.formSubmitMessage = 'Mandatory Fields Missing';
                    //   formValidate = false;
                } else {

                    $scope.hoistFormData.companyId = $scope.hoistFormData.companyId.originalObject.id || null;
                    $scope.hoistFormData.locationId = $scope.hoistFormData.locationId.originalObject.id || null;
                }
            }

            if ($scope.hoistFormData.controllerId === null || $scope.hoistFormData.controllerId === undefined || $scope.hoistFormData.controllerId.originalObject === undefined) {
                // $scope.hoistFormData.controllerId = '';
            } else {
                $scope.hoistFormData.controllerId = $scope.hoistFormData.controllerId.originalObject.id || $scope.originalHoistFormData.controllerId;
            }
            if ($scope.hoistFormData.hoistType === null || $scope.hoistFormData.hoistType === undefined || $scope.hoistFormData.hoistType.originalObject === undefined) {
                // $scope.hoistFormData.controllerId = '';
            } else {
                $scope.hoistFormData.hoistType = $scope.hoistFormData.hoistType.originalObject.id || $scope.originalHoistFormData.hoistType;
            }
            function checkForChanges(a, b) {
                // function check(a, b) {
                //     for (var attr in a) {
                //         if (a.hasOwnProperty(attr) && b.hasOwnProperty(attr)) {
                //             if (a[attr] === null|| b[attr] === null) {
                //                 return true;
                //             }
                //             else if((a[attr] === null)){
                //                 return false;
                //             }
                //             else if (a[attr] != b[attr]) {
                //                 switch (a[attr].constructor) {
                //                     case Object:
                //                         return equal(a[attr], b[attr]);
                //                     case Function:
                //                         if (a[attr].toString() != b[attr].toString()) {
                //                             return false;
                //                         }
                //                         break;
                //                     default:
                //                         return false;
                //                 }
                //             }
                //         } else {
                //             return false;
                //         }
                //     }
                //     return true;
                // };
                // return check(a, b) && check(b, a);
                if (JSON.stringify(a) == JSON.stringify(b)) {
                    return true;
                } else {
                    return false;
                }
            };
            console.log($scope.originalHoistFormData, $scope.hoistFormData, checkForChanges($scope.originalHoistFormData, $scope.hoistFormData));
            if (!formValidate) {
                //do nothing
            }
            else if (checkForChanges($scope.originalHoistFormData, $scope.hoistFormData)) {
                //$scope.formSubmitMessage = 'No Changes Made. Closing Form..';
                // setTimeout(function(){
                $scope.showModal = false;
                //},500) 
                // $scope.showMessageModalHandler('No Changes Made', 'info');
            } else {
                $scope.dataLoading = true;
                $scope.showModal = false;
                $scope.selectedIndex = null;
                if ($scope.formAction === 'add') {

                    HoistsService.addHoist($scope.hoistFormData).then(
                        function (response) {
                            if (response.error == undefined && response.exceptionMessage === undefined && response != 'Internal Server Error') {
                                $scope.showMessageModalHandler("hoist number " + $scope.hoistFormData.hoistNumber + " successfully added", 'success');
                                //alert("hoist successfully added");

                                //  $scope.hoistFormData = null;
                                $scope.getMoreData();
                                //  $scope.dataLoading = false;
                            } else if (response.code === 500) {
                                $scope.showMessageModalHandler("fail to add hoist number " + $scope.hoistFormData.hoistNumber + " with server error" + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 400) {
                                $scope.showMessageModalHandler("fail to add hoist number " + $scope.hoistFormData.hoistNumber + " with no id supplied", 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 404) {
                                $scope.showMessageModalHandler("fail to add hoist number " + $scope.hoistFormData.hoistNumber + " with no host " + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 409) {
                                $scope.showMessageModalHandler("fail to add hoist number " + $scope.hoistFormData.hoistNumber + " with conflict " + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else {
                                $scope.showMessageModalHandler("fail to add hoist number " + $scope.hoistFormData.hoistNumber, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                        },
                        function () {
                            $scope.showMessageModalHandler("failed to add hoist", 'error');
                            // alert("fail to add hoist");
                            $scope.showModal = false;
                            $scope.dataLoading = false;
                        });
                } else if ($scope.formAction === 'edit') {
                    HoistsService.editHoist($scope.hoistFormData).then(
                        function (response) {
                            if (response.error == undefined && response.exceptionMessage === undefined &&  response !== 'Internal Server Error') {
                                $scope.showMessageModalHandler("hoist Number " + $scope.hoistFormData.hoistNumber + " successfully edited", 'success');
                                // alert("hoist successfully edited");
                                $scope.showModal = false;
                                //$scope.hoistFormData = null;"
                                $scope.getMoreData();
                                //  $scope.dataLoading = false;
                            } else if (response.code === 500) {
                                $scope.showMessageModalHandler("fail to edit hoist number " + $scope.hoistFormData.hoistNumber + " with server error" + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 400) {
                                $scope.showMessageModalHandler("fail to edit hoist number " + $scope.hoistFormData.hoistNumber + " with no id supplied", 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 404) {
                                $scope.showMessageModalHandler("fail to edit hoist number " + $scope.hoistFormData.hoistNumber + " with no host " + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else if (response.code === 409) {
                                $scope.showMessageModalHandler("fail to edit hoist number " + $scope.hoistFormData.hoistNumber + " with conflict " + response.code, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                            else {
                                $scope.showMessageModalHandler("fail to edit hoist number " + $scope.hoistFormData.hoistNumber, 'error');
                                //  alert("fail to delete hoist");
                                $scope.dataLoading = false;
                            }
                        },
                        function () {
                            $scope.dataLoading = false;
                            $scope.showModal = false;
                        });
                }
                $scope.reloadAutoCompleteData();
                $scope.autoCompleteDataUnTouched = true;
            }
        }

         $scope.reallyDelete = function(hoistId, hostnumber) {
                $scope.selectedIndex = null;
                $scope.showModal = false;
            HoistsService.deleteHoist(hoistId).then(
                    function (response) {
                        if (response.error == "") {
                            $scope.showMessageModalHandler("hoist number " + hostnumber + " successfully deleted", 'success');
                            // alert("hoist data deleted");
                            $scope.dataLoading = false;
                            $scope.getMoreData();
                            //  $scope.dataLoading = false;
                        } else if (response.code === 500) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with server error" + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 400) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with no id supplied", 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 404) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with no host " + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 409) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with conflict " + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }

                        else if (response == "") {
                            $scope.showMessageModalHandler("hoist number " + hostnumber + " successfully deleted", 'success');
                            // alert("hoist data deleted");
                            $scope.dataLoading = false;
                            $scope.getMoreData();
                            //  $scope.dataLoading = false;
                        }
                        else {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                         $scope.dataLoading = false;
                    });
         };


        /*$scope.deleteHoist = function (hoistId, hostnumber) {
            $scope.selectedIndex = null;
            $scope.showModal = false;
            if (confirm('Are you sure you want to delete the hoist?')) {
                // Save it!

                $scope.dataLoading = true;
                HoistsService.deleteHoist(hoistId).then(
                    function (response) {
                        if (response.error === "") {
                            $scope.showMessageModalHandler("hoist number " + hostnumber + " successfully deleted", 'success');
                            // alert("hoist data deleted");
                            $scope.dataLoading = false;
                            $scope.getMoreData();
                            //  $scope.dataLoading = false;
                        } else if (response.code === 500) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with server error" + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 400) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with no id supplied", 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 404) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with no host " + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else if (response.code === 409) {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber + " with conflict " + response.code, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }
                        else {
                            $scope.showMessageModalHandler("fail to delete hoist number " + hostnumber, 'error');
                            //  alert("fail to delete hoist");
                            $scope.dataLoading = false;
                        }

                    });
            } else {
                // Do nothing!
                $scope.dataLoading = false;
            }
        }*/
        $scope.globalClickHandler = function () {
            $scope.selectedIndex = null;
        }
        $scope.closeModal = function () {
            $scope.showMessageModal = false;
        }

        $scope.showMessageModalHandler = function (message, type) {
            $scope.showModal = false;
            $scope.showMessageModal = true;
            $scope.messageModalText = message;
            $scope.messageModalType = type;

        }
        var focused = false;

        $scope.clearAutoComplete = function (id) {
            $scope.$broadcast('angucomplete-alt:clearInput', id);
            if (focused) {
                $('#' + id + '_value').blur();
                focused = false;
            } else {

                $('#' + id + '_value').focus();
                focused = true
            }

        }
        $scope.focusIn = function (id) {
            if ($('#' + id + '_value').val().includes("Select")) {
                $scope.$broadcast('angucomplete-alt:clearInput', id);
            }
        }
    }
]);