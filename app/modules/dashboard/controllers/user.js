﻿

dashboard.controller("UserController", ['$rootScope', '$scope', '$state', '$location', 'UsersService', 'Flash',
    function ($rootScope, $scope, $state, $location, UsersService, Flash) {
        $scope.dataLoading = true;
        $scope.showModal = false;
        $scope.selectedIndex = null
        UsersService.users().then(function (response) {
            console.log(response);
            $scope.usersData = response;
            $scope.dataLoading = false;
        }, function (msg) {
            console.log(msg);
            $scope.dataLoading = false;
        });
        UsersService.companiesList().then(function (response) {
            $scope.companyData = response;
            // $scope.dataLoading = false;
        }, function (msg) {
            //  console.log(msg);
            //  $scope.dataLoading = false;
        });

        $scope.addUser = function () {
            $scope.showModal = !$scope.showModal;
        }
        $scope.showUserRowMenu = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            console.log($event);
            $scope.selectedIndex == index ? $scope.selectedIndex = null : $scope.selectedIndex = index;

        };
    }]);


