﻿
dashboard.controller("controllerCtrl", ['$rootScope', '$scope', '$state', '$location', 'ControllersService',
 'Flash', '$timeout', '$sessionStorage',
function ($rootScope, $scope, $state, $location, ControllersService, Flash, $timeout, $sessionStorage) {
	$scope.controllersData = [];
	$scope.dataLoading = true;
	ControllersService.controllers().then(function(response) {
		$scope.controllersData = response;
		$scope.dataLoading = false;
	}, function(msg) {
		console.log(msg);
		$scope.dataLoading = false;
	});


	
	
	$scope.controllerStatuses = [];
	// column to sort
                $scope.column = 'hoistNumber';
            // sort ordering (Ascending or Descending). Set true for desending
            $scope.reverse = false;
 			//$sessionStorage.csortOrderd = false;
            // called on header click
            $scope.sortColumn = function(col){
				debugger;
                $scope.reverse = ($scope.column === col) ? !$scope.reverse : false;
                $scope.column  = col;

               
                $sessionStorage.sortOrderd=$scope.reverse;  
                $sessionStorage.sortcol=$scope.column;  
            };
            if( $sessionStorage.sortOrderd != null && $sessionStorage.sortcol!= null){
                 $scope.reverse =$sessionStorage.csortOrderd;
                 $scope.column =  $sessionStorage.csortcol;
            }

	$scope.getById = function(id) {
		for (var i = 0; i < $scope.controllerStatuses.length; i += 1){
		    var result = $scope.controllerStatuses[i];
		    if (result.id === id){
		        return result;
		    }
		}
	};	
	$scope.updateStatus = function() {
		$scope.dataLoading = false;
		var controllerIds = $scope.controllersData.map(function(a) {
			return a.controllerId;
		});
		ControllersService.updateStatus(controllerIds).then(function(response) {
			console.log(response);
			$scope.controllerStatuses = response;
			$scope.dataLoading = false;
		}, function() {
			console.log(msg);
			$scope.dataLoading = false;
		});
	};

	var updateStatusTimeout = function() {
		$scope.updateStatus();
		promise = $timeout(updateStatusTimeout, 5 * 1000);
	};

	var promise = $timeout(updateStatusTimeout, 5 * 1000);

	$scope.$on('$destroy', function(){
	    $timeout.cancel(promise);
	});

}]);

