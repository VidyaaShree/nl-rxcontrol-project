dashboard
		.controller(
				"DashboardController",
				[
						'$rootScope',
						'$scope',
						'$state',
						'$location',
						'HoistsService',
						'dashboardService',
						'Flash',
						'$timeout',
						'$interval',
						'$localStorage',
						'$sessionStorage',
						'$cookieStore',
						function($rootScope, $scope, $state, $location,
								HoistsService, dashboardService, Flash,
								$timeout, $interval, $localStorage,
								$sessionStorage, $cookieStore) {

							$scope.Events = {};
							$scope.tab = 1;

							$scope.hoistsStatusDashboard = {};

							/**
							 * Build the dynamic panels.
							 */
							$scope.dataLoading = true;
							HoistsService
									.hoistDashboard($state.params.hoistId)
									.then(
											function(response) {
												$scope.hoistsDashboardPanels = response.panels;
												$scope.dataLoading = false;
											}, function(msg) {
												console.log(msg);
												$scope.dataLoading = false;
											});

							$scope.dataLoading = true;
							HoistsService.hoist($state.params.hoistId).then(
									function(response) {
										$scope.hoist = response;
										$scope.dataLoading = false;
									}, function(msg) {
										$scope.dataLoading = false;
									});

							/**
							 * Fetch static info.
							 */
							$scope.dataLoading = true;
							HoistsService
									.hoistDashboardstatusItem(
											$state.params.hoistId)
									.then(
											function(response) {
												$scope.hoistsDashboardstatusItem = response;
												$scope.dataLoading = false;
											}, function(msg) {
												$scope.dataLoading = false;
											});

							/**
							 * Keep the statuses in a scoped variable.
							 */
							$scope.panelValues = {};
							$scope.panelStatuses = "";
							$scope.status = {
								id : 0,
								led : "grey",
								value : null,
								type : 1,
								label : "",
								labelText : ""
							};

							/**
							 * Callback from template.
							 */
							$scope.getFieldById = function(item) {
								if ($scope.panelStatuses !== "") {
									$scope.status.id = parseInt(item.id.substring(1));

									$scope.status.led = $scope.panelStatuses
										.charAt($scope.status.id) == "1" ? "red" : "green";
								} else if ($scope.panelValues !== null && $scope.panelValues[item.label] !== undefined) {
									if (item.type === 1) {
										$scope.status.led = $scope.panelValues[item.label].value ? "red" : "green";
									} else {
										$scope.status.value = $scope.panelValues[item.label].value;
									}
								}

								return $scope.status;
							}

							/**
							 * Polling methods.
							 */
							$scope.updateStatusDashboard = function() {
								HoistsService
										.updateStatusDashboard(
												$state.params.hoistId)
										.then(
												function(response) {
													$scope.hoistsStatusDashboard = response;
													$scope.panelStatuses = $scope.hoistsStatusDashboard.status == null ? ""
															: $scope.hoistsStatusDashboard.status
																	.split("")
																	.reverse()
																	.join("");
													$scope.panelValues = $scope.hoistsStatusDashboard.panelValues == null ? {}
															: $scope.hoistsStatusDashboard.panelValues;
												}, function() {
										});

							};

							$scope.updateLogItems = function() {
								// $scope.dataLoading = true;
								HoistsService
										.hoistDashboardlogItems(
												$state.params.hoistId)
										.then(
												function(response) {
													$scope.hoistsDashboardLogItems = response;
													// $scope.dataLoading = false;
												}, function() {
													// $scope.dataLoading = false;
												});
							};

							var updateStatusDashboardPromise = null;
							var updateStatusDashboardTimeout = function() {
								$scope.updateStatusDashboard();
								updateStatusDashboardPromise = $timeout(updateStatusDashboardTimeout,
										2 * 1000);
							};
							
							var updateLogItemsPromise = null;
							var updateLogItemsTimeout = function() {
								$scope.updateLogItems();
								updateLogItemsPromise = $timeout(updateLogItemsTimeout,
										10 * 1000);
							};
							
							updateStatusDashboardTimeout();
							updateLogItemsTimeout();

							$scope.$on('$destroy', function() {
								$timeout.cancel(updateStatusDashboardPromise);
								$timeout.cancel(updateLogItemsPromise);
							});
						} ]);
