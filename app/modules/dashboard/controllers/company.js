﻿

dashboard.controller("companyController", ['$rootScope', '$scope', '$state', '$location', 'CompanyService', 'Flash',
function ($rootScope, $scope, $state, $location, CompanyService, Flash) {
    $scope.dataLoading = true;
  CompanyService.companies().then(function(response) {
    console.log(response);
    $scope.companyData = response;
    $scope.dataLoading = false;
  }, function(msg) {
    console.log(msg);
    $scope.dataLoading = false;
  });

}]);

