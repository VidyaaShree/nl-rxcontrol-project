﻿

dashboard.controller("logoutCtrl", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash', '$http','$sessionStorage',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, $http, $sessionStorage) {
	$sessionStorage.$reset();
	$location.url('/login');
    
}]);

