﻿dashboard.controller("locationController",
		[
				'$rootScope',
				'$scope',
				'$state',
				'$location',
				'LocationService',
				'Flash',
				function($rootScope, $scope, $state, $location,
						LocationService, Flash) {
					$scope.dataLoading = true;
					LocationService.locations().then(function(response) {
						console.log(response);
						$scope.locationData = response;
						$scope.dataLoading = false;
					}, function(msg) {
						console.log(msg);
						$scope.dataLoading = false;
					});
				} ]);
