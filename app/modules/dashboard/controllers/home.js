﻿dashboard.controller("HomeController", [
		'$rootScope',
		'$scope',
		'$state',
		'$location',
		'dashboardService',
		'MainService',
		'Flash',
		function($rootScope, $scope, $state, $location, dashboardService,
				MainService, Flash) {
			var vm = this;

			$scope.dataLoading = true;
			$scope.mainData = {
				fullName : '',
				lastLogin : '',
				version : '',
				hoistCount : 0,
				controllerCount : 0,
				companyCount : 0,
				locationCount : 0,
				userCount : 0
			};

			MainService.main().then(function(response) {
				console.log(response);
				$scope.mainData = response;

				vm.home.mainData = [ {
					title : "Hoists",
					value : response.hoistCount,
					theme : "aqua",
					icon : "puzzle-piece",
					link : "/#/app/Hoists",
					readAccess : 'showHoists'
				}, {
					title : "Users",
					value : response.userCount,
					theme : "red",
					icon : "graduation-cap",
					link : "/#/app/user",
					readAccess : 'showUsers'
				}, {
					title : "Controllers",
					value : response.controllerCount,
					theme : "green",
					icon : "suitcase",
					link : "/#/app/controller",
					readAccess : 'showControllers'
				}, {
					title : "Companies",
					value : response.companyCount,
					theme : "yellow",
					icon : "file-code-o",
					link : "/#/app/company",
					readAccess : 'showCompanies'
				}, {
					title : "Locations",
					value : response.locationCount,
					theme : "yellow",
					icon : "globe",
					link : "/#/app/location",
					readAccess : 'showLocations'
				} ];

				$scope.dataLoading = false;
			}, function(msg) {
				console.log(msg);
				$scope.dataLoading = false;
			});

			vm.showDetails = true;
			vm.home = {};
			
			// Tools I use Carousel
			$("#owl-demo").owlCarousel({

				items : 8, // 10 items above 1000px browser width
				itemsDesktop : [ 1000, 5 ], // 5 items between 1000px
				// and 901px
				itemsDesktopSmall : [ 900, 3 ], // betweem 900px and
				// 601px
				itemsTablet : [ 600, 2 ], // 2 items between 600 and 0
				itemsMobile : false, // itemsMobile disabled -
			// inherit from itemsTablet
			// option
			});
			$("#owl-demo").trigger('owl.play', 2000);

			// Custom Navigation Events
			$(".next").click(function() {
				$("#owl-demo").trigger('owl.next');
			})
			$(".prev").click(function() {
				$("#owl-demo").trigger('owl.prev');
			})
			$(".play").click(function() {
				$("#owl-demo").trigger('owl.play', 1000); // owl.play
				// event
				// accept
				// autoPlay
				// speed as
				// second
				// parameter
			})
			$(".stop").click(function() {
				$("#owl-demo").trigger('owl.stop');
			})

			// cartoon photo slider carosusel
			$("#owl-single").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				singleItem : true,
				autoPlay : 5000, // Set AutoPlay to 3 seconds
			});
		} ]);
