﻿﻿'use strict';

dashboard.service('dashboardService', ['$http', '$q', 'Flash',
	function ($http, $q, Flash) {

		var dashboardService = {};

		return dashboardService;

	}])

dashboard.service('AuthInterceptor', [
	'$cookieStore',
	'$rootScope',
	function ($cookieStore, $rootScope) {
		var service = this;

		service.request = function (config) {

			if ($rootScope.globals || $cookieStore.get('globals')) {
				$rootScope.globals = $cookieStore.get("globals");
				config.headers.Authorization = 'Bearer '
					+ $rootScope.globals.currentUser.token;
			} else {
				config.headers.Authorization = 'Bearer ';
			}

			return config;
		};

		service.responseError = function (response) {
			if (response.status === 401) {
				$rootScope.$broadcast('notauthenticated');
			}
			return response;
		};
	}]);

dashboard
	.factory(
	'HoistsService',
	function ($http) {

		return {
			hoists: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/hoists').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			hoist: function (hoistId) {
				var promise = $http
					.get('https://scooby.centurus.it:8090/secure/hoists/' + hoistId).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			updateStatus: function (hoistIds) {
				var promise = $http.post('https://scooby.centurus.it:8090/secure/status/hoists',
					hoistIds).then(function (response) {
						return response.data;
					});

				return promise;
			},
			updateStatusDashboard: function (hoistId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/status/dashboard/' + hoistId)
					.then(function (response) {
						return response.data;
					});

				return promise;
			},
			hoistDashboard: function (hoistId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/hoists/dashboard/' + hoistId)
					.then(function (response) {
						return response.data;
					});

				return promise;
			},
			hoistDashboardlogItems: function (hoistId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/status/logitems/' + hoistId).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			hoistDashboardstatusItem: function (hoistId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/status/hoists/' + hoistId).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			addHoist: function (payLoad) {
				var promise = $http.post(
					'https://scooby.centurus.it:8090/secure/hoists',payLoad).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			editHoist: function (payLoad) {
				var promise = $http.put(
					'https://scooby.centurus.it:8090/secure/hoists',payLoad).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			deleteHoist: function (hoistId) {
				var promise = $http.delete(
					'https://scooby.centurus.it:8090/secure/hoists/' + hoistId).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			companiesList: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/companies/list').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			locationsList: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/locations/list').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			controllersList: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/controllers/list').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			hoistTypesList: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/hoists/listTypes').then(
					function (response) {
						return response.data;
					});

				return promise;
			}
		}
	})
	.factory(
	'ControllersService',
	function ($http) {

		return {
			controllers: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/controllers')
					.then(function (response) {
						return response.data;
					});

				return promise;
			},
			controller: function (controllerId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/controllers' + controllerId).then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			updateStatus: function (hoistIds) {
				var promise = $http.post(
					'https://scooby.centurus.it:8090/secure/status/controllers', hoistIds)
					.then(function (response) {
						return response.data;
					});

				return promise;
			}
		}
	})

	.factory(
	'UsersService',
	function ($http) {

		return {
			users: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/users').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			user: function (userId) {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/users' + userId)
					.then(function (response) {
						return response.data;
					});

				return promise;
			},
			companiesList: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/companies/list').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
		}
	})

	.factory(
	'MainService',
	function ($http) {

		return {
			main: function (userId) {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/main')
					.then(function (response) {
						return response.data;
					});

				return promise;
			}
		}
	})

	.factory(
	'CompanyService',
	function ($http) {

		return {
			companies: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/companies').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			company: function (companyId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/companies' + companyId).then(
					function (response) {
						return response.data;
					});

				return promise;
			}
		}
	})

	.factory(
	'LocationService',
	function ($http) {
		return {
			locations: function () {
				var promise = $http.get('https://scooby.centurus.it:8090/secure/locations').then(
					function (response) {
						return response.data;
					});

				return promise;
			},
			location: function (locationId) {
				var promise = $http.get(
					'https://scooby.centurus.it:8090/secure/locations' + locationId).then(
					function (response) {
						return response.data;
					});

				return promise;
			}
		}
	})

	.factory(
	'AuthenticationService',
	[
		'$http',
		'$cookieStore',
		'$rootScope',
		function ($http, $cookieStore, $rootScope) {
			var service = {};

			service.Login = function (username, password,
				callback) {

				$http.post('https://scooby.centurus.it:8090/login', {
					username: username,
					password: password
				}).then(function (response) {
					callback(response);
				}, function (response) {
					callback(response);
				});
			};

			service.Logout = function (callback) {
				$http.post('https://scooby.centurus.it:8090/logout').success(
					function (response) {
						callback(response);
					}).error(function (response) {
						callback(response);
					});
			};

			service.SetCredentials = function (username, token) {

				$rootScope.globals = {
					currentUser: {
						username: username,
						token: token
					}
				};

				$http.defaults.headers.common['Authorization'] = "Bearer "
					+ token; // jshint ignore:line
				$cookieStore.put('globals', $rootScope.globals);
			};

			service.setUserAccess = function (token) {
				return new Promise(function (resolve, reject) {


					var base64Url = token.split('.')[1];
					var base64 = base64Url.replace('-', '+').replace('_', '/');
					var accessObject = JSON.parse(window.atob(base64));
					$rootScope.userAccess = accessObject;
					// {
					// 	"roles" : [
					// 		"User",
					// 		//"Admin"
					// 	],
					// 	"permissions": [
					// 		"editControllers",
					// 		"editCompanies",
					// 		"showUsers",
					// 		"removeControllers",
					// 		"editPLCTypes",
					// 		"showCompanies",
					// 		"editHoistStatus",
					// 		//"showControllers",
					// 		"removeCompanies",
					// 		"removeUsers",
					// 		"showLogBits",
					// 		//"addHoists",
					// 		//"addLocations",
					// 		"showHoists",
					// 		"editHoists",
					// 		"showReports",
					// 		"connectHoists",
					// 		"editUsers",
					// 		//"addCompanies",
					// 		//"addUsers",
					// 		"editLogBits",
					// 		"showPLCTypes",
					// 		"editLocations",
					// 		"showLocations",
					// 		"showHoistsMap",
					// 		"removeLocations",
					// 		"editLogItems",
					// 		"showLogItems"
					// 	],
					// 	"sub": "pascal",
					// 	"iss": "Centurus-IT",
					// 	"iat": 1499608465,
					// 	"nbf": 1499608465,
					// 	"jti": "32f4e6c0-6ad5-4549-bbdf-ca09f33f06b8",
					// 	"exp": 1499609365
					// };
					
					resolve();
				});
			};
			service.ClearCredentials = function () {
				$rootScope.globals = {};
				$cookieStore.remove('globals');
				$http.defaults.headers.common.Authorization = 'Bearer ';
			};

			return service;
		}]);