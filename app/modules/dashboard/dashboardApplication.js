﻿var dashboard = angular.module('dashboard', ['ui.router', 'ngAnimate', 'ngMaterial', 'ngCookies']);

//dashboard.constant('HOST_URL', ''); // change as appropriate per environment
dashboard.constant('HOST_URL', 'https://scooby.centurus.it:8090');

dashboard.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider, AuthInterceptor) {

    $stateProvider.state('app.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/modules/dashboard/views/home.html',
        controller: 'HomeController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Home',
            grantAccessTo: ['User']
        }
    });

    $stateProvider.state('app.Hoists', {
        url: '/Hoists',
        templateUrl: 'app/modules/dashboard/views/hoists.html',
        controller: 'HoistController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Hoists',
            grantAccessTo: ['User'],
            readAccess: 'showHoists'
        }
    });

    $stateProvider.state('app.HoistsDash', {
        url: '/HoistsDash',
        params: {
            hoistId: null
        },
        templateUrl: 'app/modules/dashboard/views/hoistDash.html',
        controller: 'DashboardController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Hoists',
            grantAccessTo: ['User']

        }
    });

    $stateProvider.state('app.controller', {
        url: '/controller',
        templateUrl: 'app/modules/dashboard/views/controllers.html',
        controller: 'controllerCtrl',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Controllers',
            grantAccessTo: ['User'],
            readAccess: 'showControllers'
        }
    });

    $stateProvider.state('app.user', {
        url: '/user',
        templateUrl: 'app/modules/dashboard/views/users.html',
        controller: 'UserController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Users',
            grantAccessTo: ['User'],
            readAccess: 'showUsers'
        }
    
    });

$stateProvider.state('app.company', {
    url: '/company',
    templateUrl: 'app/modules/dashboard/views/company.html',
    controller: 'companyController',
    controllerAs: 'vm',
    data: {
        pageTitle: 'Companies',
        grantAccessTo: ['User'],
        readAccess: 'showCompanies'
    }
});

$stateProvider.state('app.location', {
    url: '/location',
    templateUrl: 'app/modules/dashboard/views/Locations.html',
    controller: 'locationController',
    controllerAs: 'vm',
    data: {
        pageTitle: 'Locations',
        grantAccessTo: ['User'],
        readAccess: 'showLocations'
    }
});

$stateProvider.state('app.logout', {
    controller: 'logoutCtrl'
});

$stateProvider.state('login', {
    url: '/login',
    templateUrl: 'app/modules/dashboard/views/login.html',
    controller: 'loginCtrl',
    data: {
        pageTitle: 'Login'
    }
});

$stateProvider.state('/', {
    url: '/login',
    templateUrl: 'app/modules/dashboard/views/login.html',
    controller: 'loginCtrl',
    data: {
        pageTitle: 'Login'
    }
});

$urlRouterProvider.otherwise('login');
$httpProvider.interceptors.push('AuthInterceptor');
}]);

dashboard.filter('capitalize', function () {
    return function (input, scope) {
        if (input != null)
            input = input.toLowerCase();
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
});

dashboard.directive('spinner', function () {
    return {
        restrict: 'E',
        replace: true,
        template: ' <div id="overlay"> <i class="fa fa-spinner fa-spin spin-big"></i></div>',
        link: function (scope, element, attr) {
            scope.$watch('dataLoading', function (val) {
                val = val ? $(element).show() : $(element).hide();  // Show or Hide the loading image  
            });
        }
    }
}).directive("scroll", function ($window) {
    return function (scope, element, attrs) {

        angular.element($window).bind("scroll", function () {
            // if (this.pageYOffset >= 100) {
            scope.numberToDisplay += 5;
            // }
            scope.$apply();
        });
    };
}).directive('ngReallyClick', ['$modal',
    function($modal) {

      var ModalInstanceCtrl = function($scope, $modalInstance) {
        $scope.ok = function() {
          $modalInstance.close();
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      };

      return {
        restrict: 'A',
        scope:{
          ngReallyClick:"&",
          item:"="
        },
        link: function(scope, element, attrs) {
          element.bind('click', function() {
            var message = attrs.ngReallyMessage || "Are you sure you want to delete record ?";

            /*
            //This works
            if (message && confirm(message)) {
              scope.$apply(attrs.ngReallyClick);
            }
            //*/

            //*This doesn't works
            var modalHtml = '<div class="modal-body">' + message + '</div>';
            modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

            var modalInstance = $modal.open({
              template: modalHtml,
              controller: ModalInstanceCtrl
            });

            modalInstance.result.then(function() {
              scope.ngReallyClick({item:scope.item}); //raise an error : $digest already in progress
            }, function() {
              //Modal dismissed
            });
            //*/
            
          });

        }
      }
    }
  ]);

dashboard.directive('modal', function () {
    return {
        template: '<div class="modal fade" data-keyboard="false" data-backdrop="static">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header" style="background-color: #ee7203;">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title" style="color: white;">{{ title }}</h4>' +
        '</div>' +
        '<div class="modal-body" ng-transclude style=" background-color: #f9f9f9;"></div>' +
        '</div>' +
        '</div>' +
        '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
