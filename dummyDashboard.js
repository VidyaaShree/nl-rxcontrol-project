'use strict';

dashboard.service('dashboardService', [ '$http', '$q', 'Flash',
        function($http, $q, Flash) {

            var dashboardService = {};

            return dashboardService;

        } ])

dashboard.service('AuthInterceptor', [
        '$cookieStore',
        '$rootScope',
        function($cookieStore, $rootScope) {
            var service = this;

            service.request = function(config) {

                if ($rootScope.globals || $cookieStore.get('globals')) {
                    $rootScope.globals = $cookieStore.get("globals");
                    config.headers.Authorization = 'Bearer '
                            + $rootScope.globals.currentUser.token;
                } else {
                    config.headers.Authorization = 'Bearer ';
                }

                return config;
            };

            service.responseError = function(response) {
                if (response.status === 401) {
                    $rootScope.$broadcast('notauthenticated');
                }
                return response;
            };
        } ]);

dashboard
        .factory(
                'HoistsService',
                function($http) {

                    return {
                        hoists : function() {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/hoists').then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        hoist : function(hoistId) {
                            var promise = $http
                                    .get('https://scooby.centurus.it:8090/secure/hoists/' + hoistId).then(
                                            function(response) {
                                                return response.data;
                                            });

                            return promise;
                        },
                        updateStatus : function(hoistIds) {
                            var promise = $http.post('https://scooby.centurus.it:8090/secure/status/hoists',
                                    hoistIds).then(function(response) {
                                return response.data;
                            });

                            return promise;
                        },
                        updateStatusDashboard : function(hoistId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/status/dashboard/' + hoistId)
                                    .then(function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        hoistDashboard : function(hoistId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/hoists/dashboard/' + hoistId)
                                    .then(function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        hoistDashboardlogItems : function(hoistId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/status/logitems/' + hoistId).then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        hoistDashboardstatusItem : function(hoistId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/status/hoists/' + hoistId).then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        }

                    }
                })
        .factory(
                'ControllersService',
                function($http) {

                    return {
                        controllers : function() {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/controllers')
                                    .then(function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        controller : function(controllerId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/controllers' + controllerId).then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        updateStatus : function(hoistIds) {
                            var promise = $http.post(
                                    'https://scooby.centurus.it:8090/secure/status/controllers', hoistIds)
                                    .then(function(response) {
                                        return response.data;
                                    });

                            return promise;
                        }
                    }
                })

        .factory(
                'UsersService',
                function($http) {

                    return {
                        users : function() {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/users').then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        user : function(userId) {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/users' + userId)
                                    .then(function(response) {
                                        return response.data;
                                    });

                            return promise;
                        }
                    }
                })

        .factory(
                'CompanyService',
                function($http) {

                    return {
                        companies : function() {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/companies').then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        company : function(companyId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/companies' + companyId).then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        }
                    }
                })

        .factory(
                'LocationService',
                function($http) {
                    return {
                        locations : function() {
                            var promise = $http.get('https://scooby.centurus.it:8090/secure/locations').then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        },
                        location : function(locationId) {
                            var promise = $http.get(
                                    'https://scooby.centurus.it:8090/secure/locations' + locationId).then(
                                    function(response) {
                                        return response.data;
                                    });

                            return promise;
                        }
                    }
                })

        .factory(
                'AuthenticationService',
                [
                        '$http',
                        '$cookieStore',
                        '$rootScope',
                        function($http, $cookieStore, $rootScope) {
                            var service = {};

                            service.Login = function(username, password,
                                    callback) {
                                $http.post('https://scooby.centurus.it:8090/login', {
                                    username : username,
                                    password : password
                                }).then(function(response) {
                                    callback(response);
                                }, function(response) {
                                    callback(response);
                                });
                            };

                            service.Logout = function(callback) {
                                $http.post('https://scooby.centurus.it:8090/logout').success(
                                        function(response) {
                                            callback(response);
                                        }).error(function(response) {
                                    callback(response);
                                });
                            };

                            service.SetCredentials = function(username, token) {

                                console.log('username: ' + username);
                                $rootScope.globals = {
                                    currentUser : {
                                        username : username,
                                        token : token
                                    }
                                };

                                $http.defaults.headers.common['Authorization'] = "Bearer "
                                        + token; // jshint ignore:line
                                $cookieStore.put('globals', $rootScope.globals);
                            };

                            service.ClearCredentials = function() {
                                $rootScope.globals = {};
                                $cookieStore.remove('globals');
                                $http.defaults.headers.common.Authorization = 'Bearer ';
                            };

                            return service;
                        } ]);